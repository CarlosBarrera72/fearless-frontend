import React from "react";
import { useState, useEffect } from "react";

function PresentationForm() {
  const [conferences, setConfrences] = useState([]);

  const [selectedConfrence, setSelectedConfrence] = useState([]);

  const [presenterName, setPresenterName] = useState("");

  const [presenterEmail, setPresenterEmail] = useState("");

  const [companyName, setCompanyName] = useState("");

  const [title, setTitle] = useState("");

  const [synopsis, setSynopsis] = useState([]);

  const handlePresenterNameChange = (e) => {
    const value = e.target.value;
    setPresenterName(value);
  };

  const handlePresenterEmailChange = (e) => {
    const value = e.target.value;
    setPresenterEmail(value);
  };

  const handleCompanyNameChange = (e) => {
    const value = e.target.value;
    setCompanyName(value);
  };

  const handleSynopsisChange = (e) => {
    const value = e.target.value;
    setSynopsis(value);
  };

  const handleTitleChange = (e) => {
    const value = e.target.value;
    setTitle(value);
  };

  const handleConfrenceChange = (e) => {
    const value = e.target.value;
    setSelectedConfrence(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.presenter_name = presenterName;
    data.company_name = companyName;
    data.presenter_email = presenterEmail;
    data.title = title;
    data.synopsis = synopsis;
    data.conference = selectedConfrence;
    console.log(data);

    const locationUrl = `http://localhost:8000${selectedConfrence}presentations/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);

      setPresenterName("");
      setCompanyName("");
      setPresenterEmail("");
      setTitle("");
      setSynopsis("");
      setSelectedConfrence(conferences);
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8000/api/conferences/ ";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConfrences(data.conferences);
      //   console.log(data.conferences);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handlePresenterNameChange}
                  value={presenterName}
                  placeholder="Presenter name"
                  required
                  type="text"
                  name="presenter_name"
                  id="presenter_name"
                  className="form-control"
                />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handlePresenterEmailChange}
                  value={presenterEmail}
                  placeholder="Presenter email"
                  required
                  type="email"
                  name="presenter_email"
                  id="presenter_email"
                  className="form-control"
                />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleCompanyNameChange}
                  value={companyName}
                  placeholder="Company name"
                  type="text"
                  name="company_name"
                  id="company_name"
                  className="form-control"
                />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleTitleChange}
                  value={title}
                  placeholder="Title"
                  required
                  type="text"
                  name="title"
                  id="title"
                  className="form-control"
                />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea
                  onChange={handleSynopsisChange}
                  value={synopsis}
                  className="form-control"
                  id="synopsis"
                  rows="3"
                  name="synopsis"
                ></textarea>
              </div>
              <div className="mb-3">
                <select
                  required
                  name="conference"
                  id="conference"
                  className="form-select"
                  onChange={handleConfrenceChange}
                >
                  <option value="">Choose a conference</option>
                  {conferences.map((confrence) => {
                    return (
                      <option key={confrence.href} value={confrence.href}>
                        {confrence.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;
