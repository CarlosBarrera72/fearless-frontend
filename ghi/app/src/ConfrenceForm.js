import React from "react";
import { useState, useEffect } from "react";

function ConfrenceForm() {
  const [locations, setLocations] = useState([]);
  const [name, setName] = useState("");

  const [starts, setStarts] = useState("");

  const [ends, setEnds] = useState("");

  const [description, setDescription] = useState("");

  const [maximumPresentations, setMaximumPresentations] = useState(0);

  const [maximumAttendees, setMaximumAttendees] = useState(0);

  const [selectedLocation, setSelectedLocation] = useState("");

  const [success, setSuccess] = useState(false);

  const handleNameChange = (e) => {
    const value = e.target.value;
    setName(value);
  };

  const handleStartsChange = (e) => {
    const value = e.target.value;
    setStarts(value);
  };

  const handleEndsChange = (e) => {
    const value = e.target.value;
    setEnds(value);
  };

  const handleDescriptionChange = (e) => {
    const value = e.target.value;
    setDescription(value);
  };

  const handleMaximumPresentationChange = (e) => {
    const value = e.target.value;

    setMaximumPresentations(parseInt(value));
  };

  const handleMaximumAttendeesChange = (e) => {
    const value = e.target.value;
    setMaximumAttendees(parseInt(value));
  };

  const handleLocationChange = (e) => {
    const value = e.target.value;

    const segments = value.split("/");

    const nonEmptySegments = segments.filter(
      (segment) => segment.trim() !== ""
    );

    const locationId = nonEmptySegments.pop();

    setSelectedLocation(parseInt(locationId));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.max_presentations = maximumPresentations;
    data.max_attendees = maximumAttendees;
    data.location = selectedLocation;
    console.log(data);

    const locationUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();

      setSuccess(true);

      console.log(newConference);

      setName("");
      setStarts("");
      setEnds("");
      setDescription("");
      setMaximumPresentations(0);
      setMaximumAttendees(0);
      setSelectedLocation(locations);
    }
  };

  const fetchData = async () => {
    const url = "http://localhost:8000/api/locations/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            {success && (
              <div class="alert alert-success" role="alert">
                <h4 class="alert-heading">Conference Created!</h4>
                <p>
                  Aww yeah, lets get ready for this conference its going to be
                  epic!!!!
                </p>
                <hr />
                <p class="mb-0">
                  If you'd like to add more conferences please just fill out
                  form again!
                </p>
              </div>
            )}
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input
                  onChange={handleNameChange}
                  value={name}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleStartsChange}
                  value={starts}
                  placeholder="starts"
                  required
                  type="text"
                  name="starts"
                  id="starts"
                  className="form-control"
                />
                <div id="dateHelper" className="form-text">
                  Please input date like this ex. 2225-01-26
                </div>

                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleEndsChange}
                  value={ends}
                  placeholder="ends"
                  required
                  type="text"
                  name="ends"
                  id="ends"
                  className="form-control"
                />
                <div id="dateHelper" className="form-text">
                  Please input date like this ex. 2225-01-26
                </div>

                <label htmlFor="ends">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <textarea
                  onChange={handleDescriptionChange}
                  value={description}
                  placeholder="descriptions"
                  required
                  type="text"
                  name="description"
                  id="description"
                  className="form-control"
                ></textarea>
                {/* <input
                  onChange={handleDescriptionChange}
                  value={description}
                  placeholder="descriptions"
                  required
                  type="text"
                  name="description"
                  id="description"
                  className="form-control"
                /> */}
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleMaximumPresentationChange}
                  value={maximumPresentations}
                  placeholder="maximum-presentations"
                  required
                  type="number"
                  name="maximum-presentations"
                  id="maximum-presentations"
                  className="form-control"
                />
                <label htmlFor="maximum-presentations">
                  Maximum Presentations
                </label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleMaximumAttendeesChange}
                  value={maximumAttendees}
                  placeholder="maximum-attendees"
                  required
                  type="number"
                  name="maximum-attendees"
                  id="maximum-attendees"
                  className="form-control"
                />
                <label htmlFor="maximum-attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select
                  required
                  id="location"
                  className="form-select"
                  name="location"
                  onChange={handleLocationChange}
                >
                  <option value={locations}>Choose a location</option>
                  {locations.map((location) => {
                    return (
                      <option key={location.href} value={location.href}>
                        {location.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
export default ConfrenceForm;
