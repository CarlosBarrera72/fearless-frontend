import Nav from "./Nav";
import React from "react";
import AttendConferenceForm from "./AttendConfrenceForm";
import LocationForm from "./LocationFrom";
import AttendeesList from "./AttendeesList";
import ConfrenceForm from "./ConfrenceForm";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (
    <Router>
      <React.Fragment>
        <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/conferences/new" element={<ConfrenceForm />} />
          <Route
            path="/attendees"
            element={<AttendeesList attendees={props.attendees} />}
          />
          <Route path="/attendees/new" element={<AttendConferenceForm />} />
          <Route path="/locations/new" element={<LocationForm />} />
          <Route path="/presentations/new" element={<PresentationForm />} />
        </Routes>
      </React.Fragment>
    </Router>
  );
}

export default App;
