function dateFormat(dateString) {
  const date = new Date(dateString);
  const month = date.getMonth();
  const day = date.getDate();
  const year = date.getFullYear();
  return `${month}/${day}/${year}`;
}

function errorHappened(message) {
  return `<div class="alert alert-danger" role="alert">
    ${message}
  </div>`;
}

function createCard(
  name,
  description,
  locationName,
  pictureUrl,
  startDate,
  endDate
) {
  const formattedStartDate = dateFormat(startDate);
  const formattedEndDate = dateFormat(endDate);
  return `

  <div class="col-sm-6 mb-3 mb-sm-0">
  <div class="card">
    <img src="${pictureUrl}" class="card-img-top">
    <div class="card-body">
      <h5 class="card-title">${name}</h5>
      <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
      <p class="card-text">${description}</p>
      <div class="card-footer">
      <p class="card-text">
        ${formattedStartDate} -${formattedEndDate}
      </p>
    </div>
    </div>
  </div>
</div>
    `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      errorHappened("No Data");
    } else {
      const data = await response.json();
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          console.log(details);
          const title = details.conference.name;
          const description = details.conference.description;
          const locationName = details.conference.location.name;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = details.conference.starts;
          const endDate = details.conference.ends;
          const html = createCard(
            title,
            description,
            locationName,
            pictureUrl,
            startDate,
            endDate
          );
          const conferenceRow = document.querySelector("#conferenceRow");
          conferenceRow.innerHTML += html;
        }
      }
    }
  } catch (e) {
    console.error(e);
    errorHappened(e);
    // Figure out what to do if an error is raised
  }
});
